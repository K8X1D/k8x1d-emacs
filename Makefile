PROJECT_NAME := "emacs"
GUIX_EXTRA_PROFILES := "$$HOME/.guix-extra-profiles"
EMACS_PROFILE := "${GUIX_EXTRA_PROFILES}/${PROJECT_NAME}/${PROJECT_NAME}"
EMACSCLIENT := "${EMACS_PROFILE}/bin/emacsclient"
EMACS := "${EMACS_PROFILE}/bin/emacs"

install:
	bin/emacs-install.sh

update:
	bin/emacs-update.sh

upgrade:
	bin/emacs-upgrade.sh

clean:
	rm -r eln-cache

deploy:
	# ln -sf $$PWD/bin/emacs-client.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-daemon.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-install.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-launch.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-update.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-upgrade.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-client.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-gui $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-tui $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-open-terminal.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-open-file-explorer.sh $$HOME/.local/bin/
	ln -sf $$PWD/bin/emacs-open-editor.sh $$HOME/.local/bin/
