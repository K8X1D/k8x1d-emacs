#+TITLE: K8X1D-emacs
#+AUTHOR: Kevin Kaiser

* Aims
- Replicate [[file:~/.config/k8x1d-emacs/.gitignore][set-up]], but keep the dependencies to the minimum (e.g. emacs package, external package, etc.).
- Manage set-up with [[https://guix.gnu.org/cookbook/en/html_node/Guix-Profiles-in-Practice.html][guix profile]]
- use native emacs tools when possible
* TODO Set-up description
** Emacs with pgtk 
Emacs compiled with pgtk offer a better gui experience on wayland (e.g. alpha-background for opacity).
** Keybindings 
Playing around with evil-define-key... 

It would be better to define a specific ~k8x1d-map~ and add all to it (in use-package definition).
After, adding prefix such as evil leader would be possible. 
Hence config would be evil independent.

Also, function to switch or create tab was a great idea. Reimplement. 

Prob with keymap bindings...
https://www.mattduck.com/2023-08-28-extending-use-package-bind


** Terminal support

| Options/feature | term emulator | line-mode |
|-----------------+---------------+-----------|
| coterm          | X             |           |
| vterm           | x             |           |
| eat             |               |           |
| mistty          |               |           |

** Julia support

| Options/feature     | Notebook       | REPL              | LSP |
|---------------------+----------------+-------------------+-----|
| julia-repl          |                | X  (vterm or eat) |     |
| eglot (or eglot-jl) |                |                   | X   |
| julia-snail         | X              | X (vterm or eat)  | X   |
| (ob-)julia-vterm    | ob-julia-vterm | julia-vterm       |     |
| ess                 | [[https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-julia.html][ob-julia]]       | ess-julia         |     |

Notes
- ess-julia : incompatible with coterm,
- ob-julia : ob-julia (through ess don't support async), ob-julia + ob-async don't allow :session arguments...
- The simpliest setup would be julia-mode, ess-julia (repl + notebook) and eglot (lsp), but ess don't support async + session...
- The second simpliest setup is would be julia-mode, (ob-)julia-vterm (repl + notebook) and eglot (lsp). But, this need libvterm.
- The third simpliest setup is would be julia-mode and julia-snail (repl + notebook + lsp). Need also either vterm or eat. testing notebook support...
- julia-snail ob-julia don't support typical org-babel keywords (:session, :dir). But, always async.
- Another option is to transfert the notebook capabilities to jupyter, then build julia support for REPL (throug ess for example since already needed by R).
- ob-julia-vterm async is partial. Emacs hang... See https://github.com/shg/ob-julia-vterm.el/issues/26. Solved by https://github.com/stuartthomas25/ob-julia-vterm.el/tree/patch-2

** Biblography support
Citar was the typical way to go. But appear to conflict with icomplete configuration (would need transition to vertico et al.)...
Trying bibtex  with ~bibtex~search-entry~....

** Python support

| Options/feature   | Notebook        | REPL         | LSP |
|-------------------+-----------------+--------------+-----|
| python            | ob-python       | X            |     |
| (ob-)python-vterm | ob-python-vterm | python-vterm |     |
| eglot             |                 |              | X   |
| elpy              | X               | X            | X   |

** Notebook support
Aims :
- support :session
- support :async
- support most of the language


| Options/feature | session | async                                    | multilang                         |
|-----------------+---------+------------------------------------------+-----------------------------------|
| org-babel       | X       | through [[https://github.com/jackkamm/ob-session-async][ob-session-async]] (since org 9.5) | [[https://orgmode.org/worg/org-contrib/babel/languages/index.html][list]]                              |
| jupyter         | X       | X                                        | Restricted to R, julia and python |

** Evil/Viper support
While emacs base keybinding are great, one thing that is bothersome is the need to move the hand to the arrow keys to move while editing.
~C-n~ and ~C-p~ are partial replacement for that problem, but they force a weird right hand placement.

Evil + Evil-collection are great emulation tools, but they go against the general aims of the project.

Hence, Viper-mode (native vi emulation) is in test for now.

Viper mode is not integrated enough though (need to switch movement key between vi and emacs continually).

Retrying native emacs (hand placement can be changed for better confort)...

Exploring https://www.masteringemacs.org/article/effective-editing-movement...


With proper usage of movement keybindings and changing hand placement, the initial problem seems to fade away. Emacs defaults keybindings in test...
Keybindings to remember :
- ~C-n~ : next line
- ~C-p~ : previous line  
- ~C-f~ : move forward by character
- ~C-b~ : move backward by character
- ~M-f~ : move forward by word
- ~M-b~ : mover backward by word 
- ~C-a~ : move the beginning of the line
- ~C-e~ : move to the end of the line
- ~M-e~ : move to the end of sentence
- ~M-a~ : move to the start of the sentence
- ~M-v~ : scroll up
- ~C-v~ : scroll down
- ~M->~ : move to the end of the buffer
- ~M-<~ : move to the start of the buffer
- ~C-M-f~ : end of balanced expression
- ~C-M-b~ : start of balanced expression
- ~C-j~ : insert newline
- ~C-k~ : kill line 

  One major pain-inducer particularity of emacs is the ~C-f~ and ~C-b~ to move by the character since it force to extend the pinky. The other option is to using the arrow keys, but it force to either twist the right hand or lift it from the keyboard. 

  Another observation : most of the time, CTRL is pressed since most of the "task" is moving around. A thing though is that ~M-~ sometimes need to be pressed. Evil has the advantage of not needing to continually press a key (while been relevant elsewhere, e.g. custom modal approach cannot be exported elsewhere).

  Evil seems to be a more efficient and ergonomic option. Also, provide leader key configuration to remap to ~SPC~.
  
*** Source
- https://systemcrafters.net/emacs-essentials/efficient-movement-key-bindings/
- https://www.masteringemacs.org/article/effective-editing-movement
  
** Completion support
icomplete in test...
- vertical buffer completion emulate vertico experience natively (with current configuration).
- lsp completion is still to set-up... Test icomplete in-buffer completion...
Some elements of the minad completion framework are missing and feel like it.
- marginalia in the case of seeing the value of variable via ~C-h v~
- citar in not functional currently
- vertico has better default (e.g. make similar name different) (probably lack of knowledge here)
** Documentation
Native option for emacs is eldoc.





*** Sources
- [[https://www.masteringemacs.org/article/seamlessly-merge-multiple-documentation-sources-eldoc][Seamlessly Merge Multiple Documentation Sources with Eldoc]]

** Multiple editing
*** Sources
- [[http://xahlee.info/emacs/emacs/emacs_string-rectangle_ascii-art.html][Emacs: Edit Column Text, Rectangle]]

** Music support
In test, mpc.el...
But, one advantage of emms is that it comes with tagging capacities.
Also, mpc.el, for now, is unable to add music to mpd playlist. But, can change current playling song. 

* Todos [19/34]
- [X] Create profile via guix [0/0]
  - [X] Init install script
  - [X] init channels
  - [X] init manifest
  - [X] init update script
  - [X] install
- [X] Create utilities [3/3]
  - [X] launcher
  - [X] daemon
  - [X] client
- [X] Init early-init.el
- [X] Create makefile
- [X] Remove gui elements
- [X] Add bases modules [6/6]
  - [X] Add modules support
  - [X] Add completion module
  - [X] Add evil module
  - [X] Add modeline module
  - [X] add fonts module
  - [X] add tab module
- [X] Configure theme
- [X] Add org support [3/3]
  - [X] add org appear
  - [X] prettify org
  - [X] ensure mixed font
- [X] Correct gui look [2/2]
  - [X] color compilation buffer + correct characters
  - [X] add mixed font support
- [X] Configure dired
- [-] Add terminal support
  - [X] Test coterm
  - [ ] Made comparison table (eat, vterm, coterm, mistty) (vterm necessary for julia-vterm hence is the best choice)
  - [X] Add vterm support
- [X] Add buffers configuration
- [-] Add programming support [3/6]
  - [ ] python support
  - [ ] julia support
  - [ ] R support
  - [X] emacs-lisp support
  - [X] checker support
  - [X] lsp support
- [ ] Optimize eglot
  - [ ] see https://www.reddit.com/r/emacs/comments/1b25904/is_there_anything_i_can_do_to_make_eglots/
- [X] Adjust org-agenda
  - [X] eliminate duplication for upcoming task 
- [X] Add writing support
  - [X] org support
  - [X] corrector (ltex, manually)
- [ ] Org support
  - [ ] Find way to create latex preview in a async way ([[https://github.com/karthink/org-preview][org-preview]] ? [[https://abode.karthinks.com/org-latex-preview/][org develop branch ?]]) 
- [X] Add terminal module
  - [X] explore option 
- [-] Test eye-candy relevance [3/5]
  - [X] icons via nerd-icons ?
  - [X] Add highlights (todos)
  - [X] add highlight line
  - [ ] Reduce packages  number for highlight
  - [ ] ...
- [-] Explore emacs built-in package [1/2]
  - [X] explore https://github.com/emacs-tw/awesome-emacs?tab=readme-ov-file#minibuffer
  - [ ] Add navigation support
- [-] Add pass support
  - [X] pass interaction (via pass.el)
  - [ ] pass otp adding
- [ ] Add git support
  - [ ] Test vc vs magit (https://www.gnu.org/software/emacs/manual/html_node/emacs/Version-Control.html)
  - [ ] Explore https://thanosapollo.org/posts/why-i-prefer-vc-over-magit/
- [ ] Add email support
  - [ ] explore https://thanosapollo.org/posts/mu4e-guide/
- [ ] Add irc support
- [X] Add rss support
- [ ] Add scrolling support
  - [ ] test [[https://www.reddit.com/r/emacs/comments/1hwvtyo/ultrascroll_v02_scroll_emacs_like_lightning/][ultrascroll ]]
- [X] Add browser support
- [X] add treesitter support
  - [X] extract https://www.masteringemacs.org/article/how-to-get-started-tree-sitter
- [-] Add baseline support notebook
  - [X] add julia support (with ob-julia-vterm)
  - [ ] add R support (ess)
  - [X] add Python support (ob-python)
  - [X] ensure async support ([[https://github.com/jackkamm/ob-session-async][ob-session-async]] is integrated to org since 9.5)
- [ ] Add multimedia support
  - [ ] test mpc.el
- [X] Add gtd support
  - [X] implement org-agenda
  - [X] implement org-caldav
- [ ] Examen options complétion
  - [ ] corg pour org header ? https://github.com/isamert/corg.el
  - [ ] Explorer https://oremacs.com/2017/10/04/completion-at-point/
  - [ ] complétion pour org header (https://orgmode.org/manual/Completion.html)
- [ ] ...










