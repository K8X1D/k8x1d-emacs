# -*- mode: bash-ts; -*-
PROJECT_NAME="emacs"
PROJECT_DIR="$HOME/.config/$PROJECT_NAME"
GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
GUIX_EXTRA=$HOME/.guix-extra
PROJECT_CHANNELS=channels.scm
PROJECT_MANIFEST=manifest.scm
PROFILE="$GUIX_EXTRA/$PROJECT_NAME/guix"
GUIX="$PROFILE/bin/guix"

$GUIX pull \
      --allow-downgrades \
      --channels=$PROJECT_CHANNELS \
      --profile=$PROFILE

$GUIX describe --format=channels > $PROJECT_DIR/channels.scm

notify-send -a "Emacs" "Emacs" "Upgrade done"
