# -*- mode: bash-ts; -*-
PROJECT_NAME="emacs"
EMACS_DIR="$HOME/.config/$PROJECT_NAME"
"$EMACS_DIR/bin/emacs-client.sh" --create-frame --eval "(dired (getenv \"PWD\"))"
# "$EMACS_DIR/bin/emacs-client.sh" --create-frame --eval "(dirvish (getenv \"PWD\"))"
