# -*- mode: bash-ts; -*-
PROJECT_NAME="emacs"
GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
EMACS_PROFILE="$GUIX_EXTRA_PROFILES/$PROJECT_NAME/$PROJECT_NAME"
EMACS="$EMACS_PROFILE/bin/emacs"
EMACS_DIR="$HOME/.config/$PROJECT_NAME"

export EMACSLOADPATH="$EMACS_PROFILE/share/emacs/site-lisp:$EMACSLOADPATH" 
export PATH="$EMACS_PROFILE/bin/:$PATH"

# launch () {
#     $EMACS --init-directory=$EMACS_DIR --debug-init "$@"
# }
# launch

# if [ $? -ne 0 ]; then
#     STATUS="failed..."
# else
#     STATUS="succeed"
# fi
# notify-send -a "Emacs" "Emacs" "Launch $STATUS"

$EMACS --init-directory=$EMACS_DIR --debug-init -fg "#ebdbb2" -bg "#282828" "$@"


