# -*- mode: bash-ts; -*-
PROJECT_NAME="emacs"
GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
GUIX_EXTRA=$HOME/.guix-extra
PROJECT_MANIFEST=manifest.scm

update () {
    "$GUIX_EXTRA/$PROJECT_NAME/guix/bin/guix" package \
					      --manifest=$PROJECT_MANIFEST \
					      --profile="$GUIX_EXTRA_PROFILES/$PROJECT_NAME/$PROJECT_NAME"
}
update

if [ $? -ne 0 ]; then
    STATUS="failed..."
else
    STATUS="succeed"
fi
notify-send -a "Emacs" "Emacs" "Update done"
