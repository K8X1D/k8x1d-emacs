# -*- mode: bash-ts; -*-
PROJECT_NAME="emacs"
EMACS_DIR="$HOME/.config/$PROJECT_NAME"
"$EMACS_DIR/bin/emacs-client.sh" --create-frame --eval "(k8x1d/create-or-find-today-journal-entry)"
