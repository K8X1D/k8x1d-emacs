# -*- mode: bash-ts; -*-
PROJECT_NAME="emacs"
GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
GUIX_EXTRA=$HOME/.guix-extra
PROJECT_CHANNELS=channels.scm
PROJECT_MANIFEST=manifest.scm

install () {
    mkdir -p "$GUIX_EXTRA/$PROJECT_NAME"
    guix pull \
	 --allow-downgrades \
	 --channels=$PROJECT_CHANNELS \
	 --profile="$GUIX_EXTRA/$PROJECT_NAME/guix"

    mkdir -p "$GUIX_EXTRA_PROFILES/$PROJECT_NAME"
    "$GUIX_EXTRA/$PROJECT_NAME/guix/bin/guix" package \
					      --manifest=$PROJECT_MANIFEST \
					      --profile="$GUIX_EXTRA_PROFILES/$PROJECT_NAME/$PROJECT_NAME"
}
install

if [ $? -ne 0 ]; then
    STATUS="failed..."
else
    STATUS="succeed"
fi
notify-send -a "Emacs" "Emacs" "Installation $STATUS"
