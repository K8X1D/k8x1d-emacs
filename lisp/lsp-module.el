;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Native lsp support
;; FIXME : prevent flymake indication, find problematic parameter
(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :commands (eglot
	     eglot-rename
	     eglot-ensure
	     eglot-rename
	     eglot-format
	     eglot-format-buffer)
  :config
  (setq eglot-report-progress nil)  ; Prevent minibuffer spam
  ;; (setq eglot-stay-out-of '(flymake)) ;; cause probleme with eglot-ltex, indications are not shown.

  ;; Optimizations, see https://www.reddit.com/r/emacs/comments/1b25904/is_there_anything_i_can_do_to_make_eglots/
  (fset #'jsonrpc--log-event #'ignore)
  ;; (eglot-events-buffer-size 0)
  (setq eglot-sync-connect nil)
  (setq eglot-connect-timeout nil)
  (setq eglot-autoshutdown t)
  (setq eglot-send-changes-idle-time 3)
  (setq flymake-no-changes-timeout 5)
  ;;(eldoc-echo-area-use-multiline-p nil)
  (setq eglot-ignored-server-capabilities '(:documentHighlightProvider))
  (setq eglot-menu-string "lsp")
  :bind
  ("C-c l a" . eglot-code-actions)
  ("C-c l s" . eglot-shutdown)
  ("C-c l S" . eglot-shutdown-all)
  ("C-c l r" . eglot-reconnect)
  ("C-c l f" . eglot-format)
  ("C-c l F" . eglot-format-buffer)
  )

;; Better performance for eglot
(use-package eglot-booster
  :if (string= k8x1d/lsp-provider "eglot")
  :hook
  (after-init . eglot-booster-mode)
  :config
  (setq eglot-booster-no-remote-boost t)
  )

;; LSP-MODE do too much at the same time for my need.
;; LSP via LSP-mode
(use-package lsp-mode
  :disabled
  :config
  (setq lsp-headerline-breadcrumb-enable nil)
  (setq lsp-modeline-workspace-status-enable nil)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-modeline-code-actions-enable nil)
  (setq lsp-modeline-code-actions-enable nil)
  )

;; UI interface for lsp-mode
(use-package lsp-ui
  :disabled
  )

(use-package lsp-ltex
  :disabled
  :init
  (setq lsp-ltex-language "fr")
  (setq lsp-ltex-version "15.2.0"))

;; LSP support 
(use-package lsp-bridge
  :if (string= k8x1d/lsp-provider "lsp-bridge")
  :ensure nil
  :load-path "packages/lsp-bridge"
  :hook
  ;; (prog-mode . lsp-bridge-mode)
  ;; (after-init . global-lsp-bridge-mode)
  (lsp-bridge-ref-mode  . evil-insert-state)
  ;; :bind
  ;; (:map prog-mode-map
  ;; 	("<tab>" . lsp-bridge-popup-complete-menu)
  ;; 	([remap evil-lookup] . lsp-bridge-show-documentation)
  ;; 	)
  ;; (:map acm-mode-map
  ;; 	("C-k" . acm-select-prev)
  ;; 	([remap evil-insert-digraph] . acm-select-prev)
  ;; 	("C-j" . acm-select-next)
  ;; 	)
  :config
  (setq lsp-bridge-complete-manually t)
  :init
  (setq lsp-bridge-mode-lighter " lsp")
  )




(provide 'lsp-module)
;;; lsp-module.el ends here
