;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Icons support
(use-package nerd-icons
  :config
  (add-to-list 'nerd-icons-mode-icon-alist
	       '(r-mode nerd-icons-devicon "nf-dev-r" :face nerd-icons-lblue))
  (add-to-list 'nerd-icons-mode-icon-alist
	       '(r-ts-mode nerd-icons-devicon "nf-dev-r" :face nerd-icons-lblue))
  )

;; Nerd icons for minibuffer
(use-package nerd-icons-completion
  :hook (after-init . nerd-icons-completion-mode))

;; Nerd icons for dired 
(use-package nerd-icons-dired
  :hook (dired-mode . nerd-icons-dired-mode))

;; Nerd icons for ibuffer
(use-package nerd-icons-ibuffer
  :hook (ibuffer-mode . nerd-icons-ibuffer-mode))

;; Nerd icons for magit
;; replaced with native support (see https://www.reddit.com/r/emacs/comments/1imfhwi/magit_now_natively_supports_adding_icons_in_the/)
(use-package magit-file-icons
  :disabled
  :hook
  (magit-mode . magit-file-icons-mode)
  )

(provide 'icons-module)
;;; icons-module.el ends here
