;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code: 


;; Selection info module
;; inspired by https://github.com/seagle0128/doom-modeline/blob/master/doom-modeline-segments.el
(defsubst doom-modeline-column (pos)
  "Get the column of the position `POS'."
  (save-excursion (goto-char pos)
                  (current-column)))

(defun selection-infos ()
  (when (or mark-active (and (bound-and-true-p evil-local-mode)
                                  (eq evil-state 'visual)))
    (cl-destructuring-bind (beg . end)
	(if (and (bound-and-true-p evil-local-mode) (eq evil-state 'visual))
	    (cons evil-visual-beginning evil-visual-end)
	  (cons (region-beginning) (region-end)))
      (propertize
       (let ((lines (count-lines beg (min end (point-max)))))
	 (concat
	  " "
	  (cond ((or (bound-and-true-p rectangle-mark-mode)
		     (and (bound-and-true-p evil-visual-selection)
			  (eq 'block evil-visual-selection)))
		 (let ((cols (abs (- (doom-modeline-column end)
				     (doom-modeline-column beg)))
			     ))
		   (format "%dx%dB" lines cols)))
		((and (bound-and-true-p evil-visual-selection)
		      (eq evil-visual-selection 'line))
		 (format "%dL" lines))
		((> lines 1)
		 (format "%dC %dL" (- end beg) lines))
		(t
		 (format "%dC" (- end beg))))
	  " "))
       'face 'warning
       )
      )
    )
  )

;; https://occasionallycogent.com/custom_emacs_modeline/index.html
;; Keep track of selected window, so we can render the modeline differently
(defvar cogent-line-selected-window (frame-selected-window))
(defun cogent-line-set-selected-window (&rest _args)
  (when (not (minibuffer-window-active-p (frame-selected-window)))
    (setq cogent-line-selected-window (frame-selected-window))
    (force-mode-line-update)))
(defun cogent-line-unset-selected-window ()
  (setq cogent-line-selected-window nil)
  (force-mode-line-update))
(add-hook 'window-configuration-change-hook #'cogent-line-set-selected-window)
(add-hook 'focus-in-hook #'cogent-line-set-selected-window)
(add-hook 'focus-out-hook #'cogent-line-unset-selected-window)
(advice-add 'handle-switch-frame :after #'cogent-line-set-selected-window)
(advice-add 'select-window :after #'cogent-line-set-selected-window)
(defun cogent-line-selected-window-active-p ()
  (eq cogent-line-selected-window (selected-window)))



(defun k8x1d/flymake-diagnostic-count-by-type (type)
 (let ((count 0)
	(type type))
    (dolist (d (flymake-diagnostics))
      (when (= (flymake--severity type)
	       (flymake--severity (flymake-diagnostic-type d)))
	(cl-incf count)))
    ;; (format "%d" count)
    count
    )
 )

(defun k8x1d/flymake-mode-line-prefix ()
  (let* ((errors (k8x1d/flymake-diagnostic-count-by-type :error))
	 (warnings (k8x1d/flymake-diagnostic-count-by-type :warning))
	 (notes (k8x1d/flymake-diagnostic-count-by-type :note))
	 (prefix "checker"))
    
     (cond
      ((> errors 0) (propertize
		     prefix
		     'face (flymake--lookup-type-property :error
						     'mode-line-face
						     'compilation-error)))
      ((> warnings 0) (propertize
		       prefix
		       'face (flymake--lookup-type-property :warning
						     'mode-line-face
						     'compilation-error)))
      ((> notes 0) (propertize
		       prefix
		       'face (flymake--lookup-type-property :note
							    'mode-line-face
							    'compilation-error)))
      (t prefix))
    ))

(defun k8x1d/checker-modeline-module ()
  (require 'flymake)
  (when (and flymake-mode (cogent-line-selected-window-active-p))
      '("["
	(:eval (k8x1d/flymake-mode-line-prefix))
	":"
	flymake-mode-line-error-counter
	flymake-mode-line-warning-counter
	flymake-mode-line-note-counter
	"]"
	)
  ;; flymake-mode-line-format
  ;; flymake-mode-line-counters
      ))



;; TODO: Add coloration according to status
(defun k8x1d/lsp-modeline-module ()
  (require 'eglot)
  (when (eglot-managed-p)
      (format "%s" "lsp")
    ;; '("["
      ;; eglot--mode-line-format
      ;; "]")
    ))

(defun k8x1d/mode-modeline-module ()
  ;; (require 'nerd-icons)
  (when (cogent-line-selected-window-active-p)
  ;; (format "%s %s"
	  ;; (nerd-icons-icon-for-mode major-mode)
  (format "%s"
	  (format-mode-line mode-name))
  
  ))

;; TODO: add face definition
(defun k8x1d/vc-modeline-module ()
  (when (cogent-line-selected-window-active-p)
    (if (vc-mode-line (buffer-file-name))
	;; (let* ((state (format " %s" (vc-state (buffer-file-name))))) 
	(let* ((state (format "%s" (vc-state (buffer-file-name))))
	       (symbol " ")) 
	  (cond ((string= state "up-to-date")
		 ;; (format "(%s)"
		 ;; 	 (propertize
		 ;; 	  state
		 ;; 	  'face 'bold)))
		 ;; ((string= state "edited")
		 ;;  (format "(%s)"
		 ;; 	  (propertize
		 ;; 	   state
		 ;; 	   'face 'italic)))
		;; ((string= state "added")
		;;  (format "(%s)"
		;; 	 (propertize
		;; 	  state
		;; 	  'face 'italic)))
		;; ((string= state "ignored")
		;;  (format "(%s)"
		;; 	 (propertize
		;; 	  state
		;; 	  'face 'normal)))
		;; (t
		;;  (format "(%s)"
		;; 	 (propertize
		;; 	  state
		;; 	  'face 'normal)))
		 (format "%s"
			 (propertize
			  symbol
			  'face `(:foreground ,(doom-color 'green)))))
		 ((string= state "edited")
		  (format "%s"
			  (propertize
			   symbol
			   'face `(:foreground ,(doom-color 'yellow)))))
		 ((string= state "added")
		 (format "%s"
			 (propertize
			  symbol
			  'face `(:foreground ,(doom-color 'blue))))
		 ((string= state "ignored")
		 (format "%s"
			 (propertize
			  symbol
			  'face `(:foreground ,(doom-color 'grey)))))
		 (t
		 (format "%s"
			 (propertize
			  symbol
			  'face `(:foreground ,(doom-color 'orange))))
		 )
	  ))))

(defun k8x1d/file-name-modeline-module ()
  (if (buffer-file-name)
      (file-name-nondirectory (buffer-file-name))
    )
  )

(use-package emacs
  :disabled
;;  :after evil
  :custom-face
  (mode-line ((t (:height 1.0))))
  (mode-line-active ((t (:height 1.0))))
  (mode-line-inactive ((t (:height 1.0))))
  :config
  (setq-default mode-line-format
  ;; (setq mode-line-format
	'(
		;;;;;;;;;;;;;;;;;;
	  ;; Left modules ;;
		;;;;;;;;;;;;;;;;;;
	  ;; (:eval evil-mode-line-tag)
	  "%e"
	  ;; mode-line-front-space
	  ""
	  mode-line-mule-info
	  mode-line-client
	  mode-line-modified
	  mode-line-remote
	  mode-line-window-dedicated
	  
	  ;; project-mode-line-format ;; modeline should only show buffer specific info
	  mode-line-frame-identification
	  (:eval (k8x1d/file-name-modeline-module))
	  " "
	  (:eval (k8x1d/vc-modeline-module)) ;; 
	  " "
	  (:eval (k8x1d/checker-modeline-module)) ;; 
	  " "
	  (:eval (k8x1d/lsp-modeline-module))
	  ;; " "
	  ;; mode-line-position
	  (:eval (selection-infos))

		;;;;;;;;;;;;;;;;;;;
	  ;; Right modules ;;
		;;;;;;;;;;;;;;;;;;;
	  mode-line-format-right-align
	  " "
	  spinner--mode-line-construct
	  " "
	  ;; (:eval (format "%s" (format-mode-line mode-name)))
	  (:eval (k8x1d/mode-modeline-module))
	  "  "
	  ;; mode-line-misc-info ;; superflous
	  mode-line-end-spaces)
	)
  )

;; TODO: add evil support
(use-package emacs
  :disabled
  :after evil
  :config
  ;; (require 'nerd-icons)
  (setq-default mode-line-format
  ;; (setq mode-line-format
	'(
		;;;;;;;;;;;;;;;;;;
	  ;; Left modules ;;
		;;;;;;;;;;;;;;;;;;
	  ;; (:eval evil-mode-line-tag)
	)
	)
  )

;; TODO : repair
;; Hide modeline for certain window
(use-package emacs
  :config
  (add-to-list 'display-buffer-alist
               '("^\\*vterm"
                 nil
                 (window-parameters (mode-line-format . nil))))
  )

;; Use echo-area to display infos
(use-package echo-bar
  :disabled
  :hook
  (after-init . echo-bar-mode)
  :config
  (setq echo-bar-format '(global-mode-string
			  org-mode-line-string)
	)
  )


;; Simple modeline framework  
(use-package doom-modeline
  :disabled
  :hook
  (after-init . doom-modeline-mode)
  (doom-modeline-mode .  (lambda ()
			   (doom-modeline-set-modeline 'k8x1d-doom-line 'default)))
  :config
  (setq doom-modeline-buffer-state-icon nil)
  (setq doom-modeline-buffer-encoding nil)
  ;; Define your custom doom-modeline
  (doom-modeline-def-modeline 'k8x1d-doom-line
    '(eldoc bar workspace-name window-number modals matches follow buffer-info remote-host buffer-position word-count parrot selection-info)
    '(compilation objed-state misc-info persp-name battery grip irc mu4e gnus github debug repl lsp minor-modes input-method indent-info buffer-encoding major-mode process vcs checker time))
  )

;; REWRITE

(setq project-mode-line-face 'bold)
(setq project-mode-line t)
(setq mode-line-position (list "(%l,%c)"))
(setq frame-title-format '("%b"))

(setq-default mode-line-format
      '(
	;; Left
	;; mode-line-front-space
	(project-mode-line project-mode-line-format)
	" "
	;; "%b"
	frame-title-format
	mode-line-modified
	mode-line-remote
	;; " "
	;; "%f"
	" "
	mode-line-position
	" "
	"%I"

	;; Right
	mode-line-format-right-align
	;; "[" (vc-mode vc-mode) "]"
	(:eval (k8x1d/vc-modeline-module)) ;; 
	;; flymake-mode-line-format
	" "
	flymake-mode-line-counters
	mode-line-misc-info
	mode-line-modes
	mode-line-end-spaces
	))

(use-package minions
  :hook
  (after-init . minions-mode)
  )


(provide 'modeline-module)
;;; modeline-module.el ends here
