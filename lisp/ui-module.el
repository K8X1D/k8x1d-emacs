;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Simplify ui
(tool-bar-mode -1)             ; Hide the outdated icons
(scroll-bar-mode -1)           ; Hide the always-visible scrollbar
(setq inhibit-splash-screen t) ; Remove the "Welcome to GNU Emacs" splash screen
(setq use-file-dialog nil)     ; Ask for textual confirmation instead of GUI
(setq use-dialog-box nil)      ; Remove dialog box
(setq use-short-answers t)     ; y-or-n insteat of yes-or-no
(push '(menu-bar-lines . 0) default-frame-alist) ; Remove ui element from default-frame
(push '(tool-bar-lines . 0) default-frame-alist) ; Remove ui element from default-frame
(push '(vertical-scroll-bars) default-frame-alist) ; Remove ui element from default-frame
(add-to-list 'default-frame-alist '(undecorated . t)) ;; river workaround https://codeberg.org/river/wiki#woraround-for-emacs


;; Transparency
(set-frame-parameter nil 'alpha-background 80) ;; initial transparency For current frame
(add-to-list 'default-frame-alist '(alpha-background . 80)) ;; initial transparency for all new frames henceforth



(provide 'ui-module)
;;; ui-module.el ends here
