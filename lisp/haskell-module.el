;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; Syntax highlight
(use-package haskell-mode)

;; LSP
(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :hook
  (haskell-mode . eglot-ensure)
  )


;; Checker
(use-package flymake
  :hook
  (haskell-mode . flymake-mode)
  )

;; Snippets
(use-package haskell-snippets
  :hook
  (haskell-mode . yas-minor-mode)
  )

(provide 'haskell-module)
;;; haskell-module.el ends here
