;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Tramp
(use-package tramp
  :config
  (setq org-display-remote-inline-images 'cache)
  (setq tramp-default-method "ssh") ;; Faster than the default scp (for small files)
  ;; Work around for https://issues.guix.gnu.org/55443
  ;; (connection-local-set-profile-variables
  ;;  'guix-system
  ;;  '((tramp-remote-path . (tramp-own-remote-path))))

  ;; (connection-local-set-profiles
  ;;  `(:application tramp :protocol "sudo" :machine ,(system-name))
  ;;  'guix-system)

  ;; Tips to speed up connections
  (setq tramp-verbose 0)
  (setq tramp-chunksize 2000)
  (setq tramp-use-ssh-controlmaster-options nil)
  ;; allow .dir-locals
  (setq enable-remote-dir-locals t)

  ;; Don't backup remote files
  (add-to-list 'backup-directory-alist
	       (cons tramp-file-name-regexp nil))
  )

(provide 'remote-module)
;;; remote-module.el ends here
