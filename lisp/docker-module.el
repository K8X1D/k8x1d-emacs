;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax highlight
(use-package dockerfile-ts-mode
  :config
  (add-to-list 'treesit-language-source-alist
	       '(dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile"))
  )

(provide 'docker-module)
;;; docker-module.el ends here
