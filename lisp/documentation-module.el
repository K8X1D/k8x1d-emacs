;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Documentation
(use-package eldoc
  :config
  (setq eldoc-idle-delay 0)
  ;; Minibuffer clean-up
  (setq eldoc-echo-area-prefer-doc-buffer t) ;; show in buffer instead of minibuffer when possible
  (setq eldoc-echo-area-use-multiline-p nil) ;; don't increase minibuffer to show documentation
  )


;; Compose  Eglot and flymake message
(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :preface
  (defun mp-eglot-eldoc ()
    (setq eldoc-documentation-strategy
	  'eldoc-documentation-compose-eagerly))
  :hook
  (eglot-managed-mode . mp-eglot-eldoc)
  )

;; Set pop up window position
(use-package eldoc
  :preface
  (add-to-list 'display-buffer-alist
	       '("^\\*eldoc for" display-buffer-at-bottom
		 (window-height . 10)))
  (setq eldoc-documentation-strategy 'eldoc-documentation-compose-eagerly)
  ;; To test
  ;; :config
  ;; (setq eldoc-echo-area-prefer-doc-buffer t)
  ;; (setq eldoc-echo-area-use-multiline-p nil)
  ;; ;; (add-hook 'eglot-managed-mode-hook (lambda () (eldoc-mode -1)))
  ;; (setq eldoc-display-functions '(eldoc-display-in-buffer))
  ;; (setq eldoc-documentation-strategy
  ;; 	'eldoc-documentation-compose-eagerly)
  )


(provide 'documentation-module)
;;; documentation-module.el ends here
 
