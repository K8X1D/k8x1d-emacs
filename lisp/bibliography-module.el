;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Citation manager
(use-package citar
  :bind
  (:map citar-map
	("RET" . citar-run-default-action))
  :config
  (setq citar-bibliography k8x1d/bibliography) ;; set bibliography's location
  (setq citar-notes-paths k8x1d/bibliography-notes) ;; set bibliography's location
  (setq citar-open-always-create-notes nil) ;; Allow multiple notes per bibliographic entry
  (setq citar-file-open-functions '(("html" . citar-file-open-external)
				    ;; ("pdf" . citar-file-open-external)
				    (t . find-file)))
  )

;; nerd-icons support
(use-package citar
  :after nerd-icons
  :config
  (defvar citar-indicator-notes-icons
    (citar-indicator-create
     :symbol (nerd-icons-mdicon
              "nf-md-notebook"
              :face 'nerd-icons-blue
              :v-adjust -0.3)
     :function #'citar-has-notes
     :padding "  "
     :tag "has:notes"))

  (defvar citar-indicator-links-icons
    (citar-indicator-create
     :symbol (nerd-icons-octicon
              "nf-oct-link"
              :face 'nerd-icons-orange
              :v-adjust -0.1)
     :function #'citar-has-links
     :padding "  "
     :tag "has:links"))

  (defvar citar-indicator-files-icons
    (citar-indicator-create
     :symbol (nerd-icons-faicon
              "nf-fa-file"
              :face 'nerd-icons-green
              :v-adjust -0.1)
     :function #'citar-has-files
     :padding "  "
     :tag "has:files"))

  (setq citar-indicators
	(list citar-indicator-files-icons
              citar-indicator-notes-icons
              citar-indicator-links-icons))
  )

;; Bib parser
(use-package parsebib
  :disabled
  )


;; Org integration
(use-package org
  :hook
  (org-mode . citar-capf-setup)
  :config
  (require 'citar)
  (require 'citar-org)
  ;;(citar-capf-setup)
  (setq org-cite-insert-processor 'citar)
  (setq org-cite-follow-processor 'citar)
  (setq org-cite-activate-processor 'citar)
  )

;; Latex integration
(use-package tex
  ;; :ensure auctex
  :hook
  (LaTeX-mode . citar-capf-setup)
  :config
  (require 'citar-latex)
  )

;; Embark integration
(use-package citar-embark
  :hook
  (org-mode . citar-embark-mode)
  (markdown-mode . citar-embark-mode)
  (LaTeX-mode . citar-embark-mode)
  :config
  (setq citar-at-point-function 'embark-act)
  )

;; Browse and fetch references
(use-package biblio
  :bind
  ("C-c s b" . biblio-lookup)
  )

;; Bibtex
(use-package bibtex
  :bind
  ("C-c o z" . bibtex-search-entry)
  ("C-c o Z" . bibtex-search-entries)
  (:map bibtex-mode-map
	("C-c C-R" . bibtex-reformat)
	)
  :config
  (setq bibtex-files k8x1d/bibliography)
  ;; Add keyword and file field
  (setq bibtex-user-optional-fields '(("keywords" "Personnal keyword (zotero)")))
;; indentation
  (setq bibtex-align-at-equal-sign t)
  (setq bibtex-text-indentation 17)
  (setq bibtex-field-indentation 2)
  (setq bibtex-contline-indentation 18)

  (setq bibtex-dialect 'biblatex)

  )

;; Tex support
(use-package reftex
  :config
  (setq reftex-default-bibliography k8x1d/bibliography)
  )


(provide 'bibliography-module)
;;; bibliography-module.el ends here
