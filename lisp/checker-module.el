;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Checker
(use-package flymake
  :bind (:map flymake-mode-map
	      ("C-c d p" . flymake-goto-prev-error)
	      ("C-c d n" . flymake-goto-next-error)
	      ("C-c d l" . flymake-show-buffer-diagnostics)
	      )
  :config
  ;; (setq flymake-show-diagnostics-at-end-of-line 'short)
  (setq flymake-show-diagnostics-at-end-of-line nil)
  (setq flymake-no-changes-timeout 1.5)
  )

;; Flymake indicator in the margin
(use-package flymake
  :init
  ;; (setq-default right-margin-width 5)
  ;; (setq right-margin-width 5)
  (setq flymake-indicator-type 'margins)
  (setq flymake-margin-indicator-position 'right-margin)
  (setq flymake-autoresize-margins t)
  (setq flymake-mode-line-lighter "checker")
  ;; (right-margin-width 5)
  (setq flymake-margin-indicators-string
   ;; '((error "" compilation-error)
     ;; (warning "" compilation-warning)
     ;; (note "" compilation-info)))
   ;; '((error "e" compilation-error)
     ;; (warning "w" compilation-warning)
     ;; (note "n" compilation-info)))
   '((error "⨯" compilation-error)
     (warning "⚠" compilation-warning)
     (note "!" compilation-info)))
 )

;; Extend flymake support
(use-package flymake-collection)


(provide 'checker-module)
;;; checker-module.el ends here
