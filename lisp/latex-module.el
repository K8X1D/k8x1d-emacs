;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package latex
  ;; :ensure auctex
  :hook
  ;; (LaTeX-mode . visual-line-mode)
  ;; (LaTeX-mode . (lambda () (setq truncate-lines t)))
  (LaTeX-mode . display-line-numbers-mode)
  (LaTeX-mode . LaTeX-math-mode)
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (setq LaTeX-item-indent 2)
  (setq LaTeX-indent-level 4)
  (setq font-latex-fontify-script nil)
  )

;; PDF viewing configuration
(use-package latex
  :hook
  (TeX-after-compilation-finished-functions . TeX-revert-document-buffer)
  (LaTeX-mode . TeX-source-correlate-mode)        ; activate forward/reverse search to zathura
  (LaTeX-mode . TeX-PDF-mode)
  :config
  (setq TeX-view-program-selection '((output-pdf "PDF Tools")))
  ;; (setq TeX-view-program-selection '((output-pdf "Zathura")))
  (setq TeX-source-correlate-start-server t)
  )

;;;; Navigations + references
(use-package reftex
  :hook
  ((latex-mode . turn-on-reftex)
   (LaTeX-mode . turn-on-reftex)
   (reftex-mode . visual-line-mode))
  :config
  (setq reftex-plug-into-AUCTeX t)
  (setq reftex-toc-split-windows-horizontally t)
  (setq reftex-toc-max-level 4)
  (setq reftex-level-indent 1)
  (setq reftex-default-bibliography k8x1d/bibliography)
  (setq reftex-toc-split-windows-fraction 0.2)
  )


;; Treesitter
(use-package treesit
  :config
  (add-to-list 'treesit-language-source-alist
	       '(latex "https://github.com/latex-lsp/tree-sitter-latex"))
  )

;; LSP
(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :hook
  (latex-mode . eglot-ensure)
  (LaTeX-mode . eglot-ensure)
  (TeX-mode . eglot-ensure)
  :config
  ;; from https://github.com/emacs-languagetool/eglot-ltex/issues/12
  (add-to-list 'eglot-server-programs
               `((latex-mode :language-id "latex")
                 ;; . ,(eglot-alternatives '(("digestif")
                 . ,(eglot-alternatives '(("/home/k8x1d/.guix-home/profile/bin/texlab")
                                          ("/home/k8x1d/.config/emacs/.cache/lsp/ltex-ls/bin/ltex-ls")
					  ;; ("/home/k8x1d/.local/bin/ltex-ls_server")
					  )
					)))
  )

(use-package lsp-bridge
  :if (string= k8x1d/lsp-provider "lsp-bridge")
  :ensure nil
  :load-path "packages/lsp-bridge"
  :hook
  (latex-mode . lsp-bridge-mode)
  (LaTeX-mode . lsp-bridge-mode)
  (TeX-mode . lsp-bridge-mode)
  :config
  (setq lsp-bridge-tex-lsp-server "ltex-ls")
  )


(provide 'latex-module)
;;; latex-module.el ends here
