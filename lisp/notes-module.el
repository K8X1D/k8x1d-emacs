;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defvar k8x1d/shadow-journal-directory (concat k8x1d/journal-directory "/shadow"))
(use-package org
  :preface
  (defun k8x1d/today-date ()
    (format-time-string "%Y%m%d" (current-time))
    )
  (defun k8x1d/create-or-find-today-entry (directory date)
    (interactive)
    (find-file (concat directory "/" date ".org"))
    )
  (defun k8x1d/create-or-find-today-journal-entry ()
    (interactive)
    (k8x1d/create-or-find-today-entry k8x1d/journal-directory (format-time-string "%Y-%m-%d" (current-time)))
    )
  (defun k8x1d/create-or-find-today-shadow-journal-entry ()
    (interactive)
    (k8x1d/create-or-find-today-entry k8x1d/shadow-journal-directory (format-time-string "%Y%m%d" (current-time)))
    )
  (defun k8x1d/open-journal-tab ()
    (interactive)
    (k8x1d/create-or-switch-tab-then "Journal" #'k8x1d/create-or-find-today-journal-entry)
    )
  (defun k8x1d/open-journaling-tab ()
    (interactive)
    (k8x1d/create-or-switch-tab-then "Journaling" #'k8x1d/create-or-find-today-shadow-journal-entry)
    )
  :general
  (k8x1d/leader-key
    "oj"  '(k8x1d/open-journal-tab :which-key "Journal (Today)")
    "oJ"  '(k8x1d/open-journaling-tab :which-key "Shadow Journal (Today)")
    )
  :bind
  (:map k8x1d/leader-map
	("oj" ("Journal (Today)" . k8x1d/create-or-find-today-journal-entry))
	)
  )

;; TODO: configure
;; Notes manager
(use-package denote
  :config
  (setq denote-directory k8x1d/notes-directory)
  (setq denote-known-keywords '("theoretics"
				"methodics"
				"philosophy"
				"programming"
				"work"
				"development"
				"implication"))
  ;; Journal
  (setq denote-journal-extras-directory k8x1d/journal-directory)
  (setq denote-journal-extras-title-format 'day-date-month-year-24h)
  ;; Org capture integration
  ;; from https://protesilaos.com/emacs/denote#h:115b6797-f265-40e9-a603-32eeda13a7ac
  (with-eval-after-load 'org-capture
    (add-to-list 'org-capture-templates
		 '("r" "New reference (with Denote)" plain
		   (file denote-last-path)
		   (function
		    (lambda ()
		      (let ((denote-use-directory (expand-file-name "reference" (denote-directory))))
			(denote-org-capture))))
		   :no-save t
		   :immediate-finish nil
		   :kill-buffer t
		   :jump-to-captured t)))
  )

;; Notes explortation tool
(use-package denote-menu
  :general
  (k8x1d/leader-key
    "n"  '(:ignore t :which-key "Notes")
    "nl" '(list-denotes :which-key "List")
    )
  :config
  (define-key denote-menu-mode-map (kbd "c") #'denote-menu-clear-filters)
  (define-key denote-menu-mode-map (kbd "/ r") #'denote-menu-filter)
  (define-key denote-menu-mode-map (kbd "/ k") #'denote-menu-filter-by-keyword)
  (define-key denote-menu-mode-map (kbd "/ o") #'denote-menu-filter-out-keyword)
  (define-key denote-menu-mode-map (kbd "e") #'denote-menu-export-to-dired)
  )

;; Notes visualization tool
(use-package denote-explore
  :custom
  ;; Where to store network data and in which format
  (denote-explore-network-directory k8x1d/denote-exploration-directory)
  (denote-explore-network-filename "denote-network")
  ;; (denote-explore-network-keywords-ignore "<keywords list>")
  ;; (denote-explore-network-regex-ignore "<regex>")
  (denote-explore-network-format 'd3.js)
  (denote-explore-network-d3-colours 'SchemeObservable10)
  (denote-explore-network-d3-js "https://d3js.org/d3.v7.min.js")
  ;; (denote-explore-network-d3-template "<file path>")
  ;; (denote-explore-network-graphviz-header "<header strings>")
  (denote-explore-network-graphviz-filetype 'svg)
  :bind
  (;; Statistics
   ("C-c e s n" . denote-explore-count-notes)
   ("C-c e s k" . denote-explore-count-keywords)
   ("C-c e s e" . denote-explore-barchart-filetypes)
   ("C-c e s w" . denote-explore-barchart-keywords)
   ("C-c e s t" . denote-explore-barchart-timeline)
   ;; Random walks
   ("C-c e w n" . denote-explore-random-note)
   ("C-c e w r" . denote-explore-random-regex)
   ("C-c e w l" . denote-explore-random-link)
   ("C-c e w k" . denote-explore-random-keyword)
   ;; Denote Janitor
   ("C-c e j d" . denote-explore-duplicate-notes)
   ("C-c e j D" . denote-explore-duplicate-notes-dired)
   ("C-c e j l" . denote-explore-missing-links)
   ("C-c e j z" . denote-explore-zero-keywords)
   ("C-c e j s" . denote-explore-single-keywords)
   ("C-c e j r" . denote-explore-rename-keywords)
   ("C-c e j y" . denote-explore-sync-metadata)
   ("C-c e j i" . denote-explore-isolated-files)
   ;; Visualise denote
   ("C-c e n" . denote-explore-network)
   ("C-c e r" . denote-explore-network-regenerate)
   ("C-c e d" . denote-explore-barchart-degree)
   ("C-c e b" . denote-explore-barchart-backlinks))
  )


(provide 'notes-module)
;;; notes-module.el ends here
