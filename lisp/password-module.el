;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Interact with pass
(use-package pass
  :general
  (k8x1d/leader-key
   "op"  '(pass :which-key "Password Manager") 
   )
  :bind
  (:map k8x1d/leader-map
	("op" ("Pass" . pass))
	)
  )

;; Password generator (w transient)
(use-package password-gen)

;; Password generator
(use-package password-generator)


(provide 'password-module)
;;; password-module.el ends here
