;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Show completion preview in buffer
(use-package completion-preview
  :if (string= k8x1d/lsp-provider "eglot")
  :hook
  (after-init . global-completion-preview-mode)
  :bind
  (:map completion-preview-active-mode-map
	("C-j" . completion-preview-next-candidate)
	([remap evil-insert-digraph] . completion-preview-prev-candidate))
  )

;; Vertical completion in minibuffer (native)
;; Sources : https://www.rahuljuliato.com/posts/in-buffer-icomplete
;; FIXME: doesn't allow to select a document through citar; work by using exit-minibuffer with citar-open-file
(use-package icomplete
  :bind
  (:map icomplete-minibuffer-map
	("C-k" . icomplete-backward-completions)
	([remap evil-insert-digraph] . icomplete-backward-completions) 
	("C-j" . icomplete-forward-completions)
	("TAB" . icomplete-force-complete)
	("C-v" . icomplete-vertical-toggle)
	("C-<return>" . exit-minibuffer)
	("RET" . icomplete-force-complete-and-exit))

  :hook
  (after-init . (lambda ()
		  (fido-mode -1)
                  ;; (icomplete-mode 1)
                  (icomplete-vertical-mode 1)
                  ))
  :config
  (setq tab-always-indent 'complete)  ;; Starts completion with TAB
  (setq icomplete-delay-completions-threshold 0)
  (setq icomplete-compute-delay 0)
  (setq icomplete-show-matches-on-no-input t)
  (setq icomplete-hide-common-prefix nil)
  (setq icomplete-prospects-height 10)
  (setq icomplete-separator " . ")
  (setq icomplete-with-completion-tables t)
  (setq icomplete-in-buffer t)
  (setq icomplete-max-delay-chars 0)
  (setq icomplete-scroll t)
  ;; ;; Completion in buffer (similar to corfu)
  ;; (advice-add 'completion-at-point
  ;;             :after #'minibuffer-hide-completions)
  )


;; Completion in region via minibuffer
(use-package icomplete
  :config
  ;; Completion suggestion in minibuffer
  (require 'consult)
  (setq completion-in-region-function
	(lambda (&rest args)
	  (apply (if icomplete-vertical-mode
		     #'consult-completion-in-region
		   #'completion--in-region)
		 args)))
  )

;; (with-eval-after-load 'minibuffer
;;   (setq completions-format 'one-column)
;;   (keymap-set minibuffer-local-must-match-map "C-j" #'minibuffer-next-completion)
;;   (keymap-set minibuffer-local-must-match-map "C-k" #'minibuffer-previous-completion)
;;   )

;; Annotation for completion candidate
(use-package marginalia
  :hook
  (after-init . marginalia-mode)
  )
;; (use-package minibuffer
;;   :config
;;   (setq completions-detailed t)
;;   )


;; Search and navigation based on completing-read
(use-package consult
  :bind
  ([remap bookmark-jump]                 . consult-bookmark)
  ([remap evil-show-marks]               . consult-mark)
  ;; ([remap evil-show-jumps]               . +vertico/jump-list
  ([remap evil-show-registers]           . consult-register)
  ([remap goto-line]                     . consult-goto-line)
  ([remap imenu]                         . consult-imenu)
  ([remap Info-search]                   . consult-info)
  ([remap locate]                        . consult-locate)
  ([remap load-theme]                    . consult-theme)
  ([remap man]                           . consult-man)
  ([remap recentf-open-files]            . consult-recent-file)
  ([remap switch-to-buffer]              . consult-buffer)
  ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
  ([remap switch-to-buffer-other-frame]  . consult-buffer-other-frame)
  ([remap yank-pop]                      . consult-yank-pop)
  ;; ([remap persp-switch-to-buffer]        . +vertico/switch-workspace-buffer)
  )



;; Completion At Point Extensions 
(use-package cape
  ;; Bind prefix keymap providing all Cape commands under a mnemonic key.
  ;; Press C-c p ? to for help.
  :bind ("C-c p" . cape-prefix-map) ;; Alternative key: M-<tab>, M-p, M-+
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  ;; (add-hook 'completion-at-point-functions #'cape-history)
  ;; ...
)

;; Contextual action
(use-package embark
  :bind (("C-;" . embark-act)
         :map minibuffer-local-map
         ("C-c C-c" . embark-collect)
         ("C-c C-e" . embark-export)))

;; Integrate embark and consult
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; Save history in completion
(use-package savehist
  :hook
  (after-init . savehist-mode)
  )

;; Keep track of recently visited files
(use-package recentf
  :hook
  (after-init . recentf-mode)
  )



(provide 'completion-alt-module)
;;; completion-alt-module.el ends here
