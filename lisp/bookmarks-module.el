;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Bookmarks
(use-package bookmark
  :general
  (k8x1d/leader-key
   "B"  '(:ignore t :which-key "Bookmarks")
   "<return>"  '(bookmark-jump :which-key "Bookmark Jump")
   "Bs"  '(bookmark-set :which-key "Save")
   )
  )

;; Tab integration
(use-package tab-bookmark
  :general
  (k8x1d/leader-key
   "Bt"  '(tab-bookmark :which-key "Save")
   "BS"  '(tab-bookmark-save :which-key "Save tab")
   )
  )

(provide 'bookmarks-module)
;;; buffers-module.el ends here
