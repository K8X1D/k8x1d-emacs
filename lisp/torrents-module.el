;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package transmission
  :general
  (k8x1d/leader-key
   "oD"  '(transmission :which-key "Downloads")
   )
  :bind
  (:map k8x1d/leader-map
	("oD" ("Downloads" . transmission))
	)
  :config
  (setq transmission-refresh-modes '(transmission-mode
                                     transmission-files-mode
                                     transmission-info-mode
                                     transmission-peers-mode))
  )


(provide 'torrents-module)
;;; torrents-module.el ends here
