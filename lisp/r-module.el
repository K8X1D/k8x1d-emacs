;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntaxe highligh + REPL+++
(use-package ess
  :disabled)

(use-package ess-r-mode
  :disabled
  :mode "\\.R\\'"
  :config
  (setq ess-eval-visibly 'nowait)
  (setq ess-history-file nil)
  (setq-default ess-history-file nil)
  (setq ess-indent-offset 2)
  ;; Linter
  (setq ess-use-flymake t)
  (setq ess-r-flymake-lintr-cache t)
  (setq ess-r-flymake-linters
	'("closed_curly_linter = NULL"
	  "commas_linter = NULL"
	  "commented_code_linter = NULL"
	  "infix_spaces_linter = NULL"
	  "line_length_linter = NULL"
	  "object_length_linter = NULL"
	  "object_name_linter = NULL"
	  "object_usage_linter = NULL"
	  "open_curly_linter = NULL"
	  "pipe_continuation_linter = NULL"
	  "single_quotes_linter = NULL"
	  "spaces_inside_linter = NULL"
	  "spaces_left_parentheses_linter = NULL"
	  "trailing_blank_lines_linter = NULL"
	  "trailing_whitespace_linter = NULL"))
  )

;; Syntax highlight via tree-sitter
(use-package r-ts-mode
  :mode "\\.R\\'"
  :config
  (add-to-list 'treesit-language-source-alist
	       '(r "https://github.com/r-lib/tree-sitter-r"))
  (add-to-list 'major-mode-remap-alist
	       '((r-mode . r-ts-mode)))
  )

;; REPL support via vterm
(use-package R-vterm
  :general
  (k8x1d/leader-key
   "orr"  '(R-vterm-repl :which-key "R")
   )
  :load-path "local-packages/r-vterm"
  :hook
  (r-mode . R-vterm-mode)
  (r-ts-mode . R-vterm-mode)
  )

;; Checker
(use-package flymake
  :hook
  (ess-r-mode . flymake-mode)
  (r-mode . flymake-mode)
  (r-ts-mode . flymake-mode)
  )

;; LSP
(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :hook
  (ess-r-mode . eglot-ensure)
  (r-mode . eglot-ensure)
  (r-ts-mode . eglot-ensure)
  )

(use-package lsp-bridge
  :if (string= k8x1d/lsp-provider "lsp-bridge")
  :load-path "packages/lsp-bridge"
  :hook
  (ess-r-mode . lsp-bridge-mode)
  (r-mode . lsp-bridge-mode)
  (r-ts-mode . lsp-bridge-mode)
  :config
  ;; Add treesitter mode
  (add-to-list 'lsp-bridge-single-lang-server-mode-list
	       '((r-ts-mode) . "rlanguageserver"))
  )

;; Snippets
(use-package yasnippet
  :hook
  (r-mode . yas-minor-mode)
  (r-ts-mode . yas-minor-mode)
  )

;; Notebook
(use-package org
  :config
  (add-to-list 'org-babel-load-languages '(R . t))
  )

(provide 'r-module)
;;; r-module.el ends here
