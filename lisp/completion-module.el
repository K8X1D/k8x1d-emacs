;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Inspirations :
;; - https://protesilaos.com/codelog/2024-02-17-emacs-modern-minibuffer-packages/

;; General configurations
(use-package emacs
  :config
  (setq tab-always-indent 'complete)
  (setq enable-recursive-minibuffers t) ;; don't get stuck in minibuffer
  )

;; Preview completion at point
(use-package completion-preview
  :hook
  (after-init . global-completion-preview-mode)
  )


;; Mini-buffer vertical completion
(use-package icomplete
  :disabled
  :bind
  (:map icomplete-minibuffer-map
	("<tab>" . icomplete-force-complete)
	("C-n" . icomplete-forward-completions)
        ("C-p" . icomplete-backward-completions)
        ;; ("C-v" . icomplete-vertical-toggle)
        ("RET" . icomplete-force-complete-and-exit))
  :hook
  (after-init . icomplete-vertical-mode)
  :config
  ;; Annotations
  (setq suggest-key-bindings t)
  ;; (setq completions-detailed t)
  ;; (setq completion-extra-properties :category)

  ;; Ignore case
  (setq read-buffer-completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)

  (setq icomplete-show-matches-on-no-input t) ;; don't start empty when starting completion
  (setq icomplete-scroll t) ;; don't cycle through candidate
  
  ;; In test
  (setq tab-always-indent 'complete)  ;; Starts completion with TAB
  (setq icomplete-delay-completions-threshold 0)
  (setq icomplete-compute-delay 0)
  (setq icomplete-hide-common-prefix nil)
  (setq icomplete-prospects-height 40)
  (setq icomplete-separator " . ")
  (setq icomplete-with-completion-tables t)
  (setq icomplete-max-delay-chars 0)

  ;; Completion in buffer
  (setq icomplete-in-buffer t)
  (advice-add 'completion-at-point
              :after #'minibuffer-hide-completions)
  )

;; In test
(use-package icomplete
  :disabled
  :bind (:map icomplete-minibuffer-map
              ("C-n" . icomplete-forward-completions)
              ("C-p" . icomplete-backward-completions)
              ("C-v" . icomplete-vertical-toggle)
              ("RET" . icomplete-force-complete-and-exit))
  :hook
  (after-init . (lambda ()
                  (fido-mode -1)
                  (icomplete-mode 1)
                  (icomplete-vertical-mode 1)
                  ))
  :config
  (setq tab-always-indent 'complete)  ;; Starts completion with TAB
  (setq icomplete-delay-completions-threshold 0)
  (setq icomplete-compute-delay 0)
  (setq icomplete-show-matches-on-no-input t)
  (setq icomplete-hide-common-prefix nil)
  (setq icomplete-prospects-height 10)
  (setq icomplete-separator " . ")
  (setq icomplete-with-completion-tables t)
  (setq icomplete-in-buffer t)
  (setq icomplete-max-delay-chars 0)
  (setq icomplete-scroll t)
  (advice-add 'completion-at-point
              :after #'minibuffer-hide-completions)
  )

;; Evil compatibility
(use-package icomplete
  :disabled
  :after evil
  :bind
  (:map icomplete-vertical-mode-minibuffer-map
	([remap icomplete-force-complete-and-exit] . icomplete-forward-completions)
	("C-k" . icomplete-backward-completions)
	("<escape>" . keyboard-escape-quit)
	([remap minibuffer-complete] . icomplete-force-complete)
	("<return>" . icomplete-ret)
	;; TODO: add remap for shift+tab
	)
  )

;; Completion configuration
(use-package minibuffer
  :disabled
  :bind
  (:map completion-list-mode-map
	("C-k" . minibuffer-previous-completion)
	("C-j" . minibuffer-next-completion)
	;; ("<escape>" . quit-window)
	)
  :config
  (setq completion-cycle-threshold 3)
  ;; (setq completion-styles '(basic partial-completion substring))
  (setq completion-styles '(substring flex)) 
  ;; (setq completion-styles '(basic flex))
  ;; (setq completion-styles '(basic partial-completion emacs22))
  ;; (setq completion-category-overrides
  ;; 	'((buffer
  ;; 	   (styles initials flex)
  ;; 	   (cycle . 3))))
  (setq completion-auto-select t) ;; Show completion on first call
  (setq completion-auto-help 'visible) ;; Display *Completions* upon first request
  ;; (setq completions-format 'one-column) ;; Use only one column
  ;; (setq completions-format 'one-column)
  ;; (setq completions-format 'vertical)
  (setq completions-format 'horizontal)
  (setq completions-sort 'historical) ;; Order based on minibuffer history
  (setq completions-max-height 30) ;; Limit completions to 20 (completions start at line 5)
  (setq completions-header-format nil)
  (setq completion-ignore-case t) ;; ignore case
  )





;; TODO: replace with icomplete-vertical (built-in)
;; Vertical completion for complete-at-read
(use-package vertico
  :hook
  (after-init . vertico-mode)
  :config
  (setq vertico-cycle nil)
  (setq vertico-resize nil)
  (setq vertico-count 10)
  ;; Evil compatibility
  ;; :bind
  ;; (:map vertico-map
  ;; 	("?" . minibuffer-completion-help)
  ;; 	("C-j" . vertico-next)
  ;; 	("C-k" . vertico-previous)
  ;; 	("M-h" . vertico-directory-up))
  )

;; Completion in region via minibuffer
(use-package vertico
  :config
  ;; Completion suggestion in minibuffer
  (require 'consult)
  (setq completion-in-region-function
	(lambda (&rest args)
	  (apply (if vertico-mode
		     #'consult-completion-in-region
		   #'completion--in-region)
		 args)))
  )

;; Annotation for completion candidate
(use-package marginalia
  :hook
  (after-init . marginalia-mode)
  )

;; Better completion order for candidate
(use-package orderless
  :config
  (setq completion-styles '(orderless partial-completion basic)))

;; The `consult' package provides lots of commands that are enhanced
;; variants of basic, built-in functionality.  One of the headline
;; features of `consult' is its preview facility, where it shows in
;; another Emacs window the context of what is currently matched in
;; the minibuffer.  Here I define key bindings for some commands you
;; may find useful.  The mnemonic for their prefix is "alternative
;; search" (as opposed to the basic C-s or C-r keys).
;;
;; Further reading: https://protesilaos.com/emacs/dotemacs#h:22e97b4c-d88d-4deb-9ab3-f80631f9ff1d


;; Search and navigation based on completing-read
(use-package consult
  :bind
  ([remap bookmark-jump]                 . consult-bookmark)
  ([remap evil-show-marks]               . consult-mark)
  ;; ([remap evil-show-jumps]               . +vertico/jump-list
  ([remap evil-show-registers]           . consult-register)
  ([remap goto-line]                     . consult-goto-line)
  ([remap imenu]                         . consult-imenu)
  ([remap Info-search]                   . consult-info)
  ([remap locate]                        . consult-locate)
  ([remap load-theme]                    . consult-theme)
  ([remap man]                           . consult-man)
  ([remap recentf-open-files]            . consult-recent-file)
  ([remap switch-to-buffer]              . consult-buffer)
  ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
  ([remap switch-to-buffer-other-frame]  . consult-buffer-other-frame)
  ([remap yank-pop]                      . consult-yank-pop)
  ;; ([remap persp-switch-to-buffer]        . +vertico/switch-workspace-buffer)
  )

;; Completion At Point Extensions 
(use-package cape
  ;; Bind prefix keymap providing all Cape commands under a mnemonic key.
  ;; Press C-c p ? to for help.
  :bind ("C-c p" . cape-prefix-map) ;; Alternative key: M-<tab>, M-p, M-+
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  ;; (add-hook 'completion-at-point-functions #'cape-history)
  ;; ...
)

;; Contextual action
(use-package embark
  :bind (("C-;" . embark-act)
         :map minibuffer-local-map
         ("C-c C-c" . embark-collect)
         ("C-c C-e" . embark-export)))

;; Integrate embark and consult
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; Save history in completion
(use-package savehist
  :hook
  (after-init . savehist-mode)
  )

;; Keep track of recently visited files
(use-package recentf
  :hook
  (after-init . recentf-mode)
  )

(provide 'completion-module)
;;; completion-module.el ends here
