;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package org2blog
  :config
  (setq org2blog/wp-blog-alist
	'(("k8x1d-website"
	   :url "http://www.k8x1d.koumbit.org/xmlrpc.php"
	   :username "k8x1d")))
  )

(provide 'blog-module)
;;; blog-module.el ends here
