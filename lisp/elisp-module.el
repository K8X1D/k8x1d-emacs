;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax hightlight
(use-package elisp-mode)

;; Diagnostic
(use-package flymake
  :hook
  (emacs-lisp-mode . flymake-mode)
  )

(provide 'elisp-module)
;;; elisp-module.el ends here
