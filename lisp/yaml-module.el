;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Syntax highlight
(use-package yaml-ts-mode
  :mode "\\.ya?ml\\'"
  :config
    (add-to-list 'treesit-language-source-alist
	'(julia "https://github.com/tree-sitter/tree-sitter-yaml"))
  )

(provide 'yaml-module)
;;; yaml-module.el ends here
