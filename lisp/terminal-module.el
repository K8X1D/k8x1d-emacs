;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Shell
(use-package shell
  :disabled
  :bind
  ("C-c o s" . shell)
  )

;; Terminal emulator
(use-package term
  :disabled
  :bind
  ("C-c o t" . term)
  )

;; FIXME : cause problems with ess-julia
;; ;; Add terminal emulation capacities to comint
 (use-package coterm
   :disabled
   :hook
   (after-init . coterm-mode)
   (coterm-mode . coterm-auto-char-mode)
   :bind
   (:map comint-mode-map
     ("C-;" . coterm-char-mode-cycle))
   :config
   (setq-default comint-process-echoes t)
   )

;; High performance terminal emulator
(use-package vterm
  ;; :load-path "local-packages/emacs-libvterm"
  :config
  (setq vterm-max-scrollback 100000)
  ;; (setq vterm-shell "bash")

  (setq vterm-tramp-shells '((t login-shell)))
  (add-to-list 'vterm-tramp-shells '("ssh" login-shell "/bin/bash"))
  (add-to-list 'vterm-tramp-shells '("scp" login-shell "/bin/bash"))
  (add-to-list 'vterm-tramp-shells '("docker" login-shell "/bin/bash"))
  (add-to-list 'vterm-tramp-shells '("podman" login-shell "/bin/bash"))
  (add-to-list 'vterm-tramp-shells '("sudo" login-shell "/bin/bash"))
  ;; don't ask for confirmation when killing vterm buffer, see https://github.com/akermu/emacs-libvterm/issues/749
  ;; (defun my/vterm-better-kill (orig-fun &rest args)
  ;;   "Override kill-buffer for vterm: kill without confirmation when vterm is idle."
  ;;   (if (eq major-mode 'vterm-mode)
  ;; 	(let ((process (get-buffer-process (current-buffer))))
  ;; 	  (when process
  ;; 	    (if (vterm--at-prompt-p)
  ;; 		(set-process-query-on-exit-flag process nil)
  ;; 	      (set-process-query-on-exit-flag process t)))))
  ;;   (apply orig-fun args))

  ;; (advice-add 'kill-buffer :around #'my/vterm-better-kill)
  )

;; Manage multipe vterm
(use-package multi-vterm
  :general
  (k8x1d/leader-key
   "o"  '(:ignore t :which-key "Open")
   "ot"  '(multi-vterm-dedicated-toggle :which-key "Terminal (Dedicated)")
   "oT"  '(multi-vterm :which-key "Terminal")
   )
  :bind
  (:map project-prefix-map
	("t" . multi-vterm-project))
  (:map k8x1d/leader-map
	("oT" ("Terminal" . multi-vterm))
	("ot" ("Terminal (Dedicated)" . multi-vterm-dedicated-toggle))
	)
  :config
  (setq multi-vterm-dedicated-window-height 17)
  ;; Add terminal to project command
  (add-to-list 'project-switch-commands '(multi-vterm-project "Terminal" "t"))
  ;; Open terminal at the bottom
  (add-to-list 'display-buffer-alist
	       '("^\\*vterm" display-buffer-at-bottom
		 (window-height . 17)
		 (window-parameters (mode-line-format . nil))
		 ))
  )


;; Vterm compatibility for visual command in eshell
(use-package eshell-vterm
  :hook
  (eshell-mode . eshell-vterm-mode)
  )

(provide 'terminal-module)
;;; terminal-module.el ends here
