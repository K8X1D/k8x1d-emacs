;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
;; Inspirations :
;; - https://emacs.stackexchange.com/questions/65080/stop-major-modes-from-overwriting-my-keybinding

;; General keybindings
(use-package emacs
  :init
  (defvar k8x1d/leader-map (make-sparse-keymap)
    "Keymap for \"leader key\" shortcuts.")
  (defvar my-localleader-map (make-sparse-keymap)
    "Keymap for \"localleader key\" shortcuts.")
  (add-to-list 'emulation-mode-map-alists 'k8x1d/leader-map) ;; ensure that leader-map ha higher priority
  :bind*
  ;; Simplify ESC ESC ESC to ESC
  ("<escape>" . keyboard-escape-quit)
  (:map k8x1d/leader-map
	;; ("q" ("Quit" . (keymap)))
	("qq" ("Emacs" . save-buffers-kill-terminal))
	("qQ" ("Emacs AND daemon)" . save-buffers-kill-emacs))
	)
  :bind-keymap*
  ("C-a" . k8x1d/leader-map)
  :config
  (keymap-set k8x1d/leader-map "h" help-map) ;; Help remap
  (keymap-set k8x1d/leader-map "o" '("Open" . (keymap)))
  (keymap-set k8x1d/leader-map "q" '("Quit" . (keymap)))
  )

;; Show keys in minibuffer
(use-package which-key
  :hook
  (after-init . which-key-mode)
  :config
  (setq which-key-min-display-lines 10)
  (setq which-key-sort-order #'which-key-key-order-alpha)
  )

;; Set prefix
(use-package general
  :init
  (general-evil-setup)
  (general-create-definer k8x1d/leader-key
    :states '(normal insert visual emacs motion)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "C-SPC")
  )

;; Set base keybindings
(use-package emacs
  :general
  (k8x1d/leader-key
    "o"  '(:ignore t :which-key "Open")
    "or"  '(:ignore t :which-key "REPL")
    "q"  '(:ignore t :which-key "Quit")
    "qq"  '(save-buffers-kill-terminal :which-key "Emacs")
    "qQ"  '(save-buffers-kill-emacs :which-key "Emacs AND daemon")
    "h" '(:keymap help-map
		  :which-key "Help")
    )
  )

(provide 'keybindings-module)
;;; keybindings-module.el ends here
