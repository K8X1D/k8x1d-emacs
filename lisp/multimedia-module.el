;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;; Inspirations :
;; - https://sqrtminusone.xyz/posts/2021-09-07-emms/#integrating-with-emms
;; - https://emacs.stackexchange.com/questions/64532/emms-and-mpd-configuration
;;; Code:

;; Multimedia player + tag editor
(use-package emms
  :config
  (require 'emms-setup)
  (emms-all) ; don't change this to values you see on stackoverflow questions if you expect emms to work
  (emms-default-players)
  ;; Clean-up elements from (emms-all)
  (emms-mode-line-mode -1)
  (emms-playing-time-mode -1)
  )

;; Emms + Mpd interaction
(use-package emms
  :preface
  (defun k8x1d/open-music-player-tab ()
    (interactive)
    (k8x1d/create-or-switch-tab-then "Music" #'emms-smart-browse)
    )
  :general
  (k8x1d/leader-key
   "oM"  '(k8x1d/open-music-player-tab :which-key "Music player")
   )
  :bind
  (:map k8x1d/leader-map
	("oM" ("Music" . emms-smart-browse))
	)
  :config
  (require 'emms-player-mpd)
  ;; (setq emms-source-file-default-directory (expand-file-name "~/Music/"))
  (setq emms-player-mpd-server-name "localhost")
  (setq emms-player-mpd-server-port "6600")
  (setq emms-player-mpd-music-directory (expand-file-name "~/Music/"))

  (add-to-list 'emms-info-functions 'emms-info-mpd)
  (add-to-list 'emms-player-list 'emms-player-mpd)
  (emms-player-set emms-player-mpd
		   'regex
		   (emms-player-simple-regexp
		    "m3u" "ogg" "flac" "mp3" "wav" "mod" "au" "aiff"))

  (emms-player-mpd-connect)

  ;; (add-hook 'emms-playlist-cleared-hook 'emms-player-mpd-clear)
  )

;; test config for mpv-mpd interaction
(use-package emms
  :disabled
  :config
  (add-to-list 'emms-player-list 'emms-player-mpv)
  (emms-player-set emms-player-mpv
		   'regex
		   (emms-player-simple-regexp
		    "mp4"))
  )

;; Emms + Mpv interaction
;; Interact badfly with mpd set-up, find why
(use-package emms
  :disabled
  :config
  (add-to-list 'emms-player-list 'emms-player-mpv)
  (emms-player-set emms-player-mpv
		   'regex
		   (rx (or (: "https://" (* nonl) "youtube.com" (* nonl))
			   (+ (? (or "https://" "http://"))
			      (* nonl)
			      (regexp (eval (emms-player-simple-regexp
					     "mp4" "mov" "wmv" "webm" "flv" "avi" "mkv")))))))
  )

;; Emms + yt-dlp
(use-package emms
  :disabled
  :config
  (setq my/youtube-dl-quality-list
	'("bestvideo[height<=720]+bestaudio/best[height<=720]"
	  "bestvideo[height<=480]+bestaudio/best[height<=480]"
	  "bestvideo[height<=1080]+bestaudio/best[height<=1080]"))

  (setq my/default-emms-player-mpv-parameters
	'("--quiet" "--really-quiet" "--no-audio-display"))

  (defun my/set-emms-mpd-youtube-quality (quality)
    (interactive "P")
    (unless quality
      (setq quality (completing-read "Quality: " my/youtube-dl-quality-list nil t)))
    (setq emms-player-mpv-parameters
	  `(,@my/default-emms-player-mpv-parameters ,(format "--ytdl-format=%s" quality))))

  (my/set-emms-mpd-youtube-quality (car my/youtube-dl-quality-list))
  ;; Cleanup emms cache
  (defun my/emms-cleanup-urls ()
    (interactive)
    (let ((keys-to-delete '()))
      (maphash (lambda (key value)
		 (when (eq (cdr (assoc 'type value)) 'url)
		   (add-to-list 'keys-to-delete key)))
	       emms-cache-db)
      (dolist (key keys-to-delete)
	(remhash key emms-cache-db)))
    (setq emms-cache-dirty t))
  )


;; Cycle emms modeline
;; Showed in status bar instead
(use-package emms-mode-line-cycle
  :disabled
  :hook
  (emms-mode-line-mode . emms-mode-line-cycle)
  (emms-mode-line-mode . emms-playing-time-mode)
  :config
  ;; `emms-mode-line-cycle' can be used with emms-mode-line-icon.
  (require 'emms-mode-line-icon)
  (setq emms-mode-line-cycle-use-icon-p nil)
  )

;; MPV interface
(use-package ready-player
  :disabled
  :hook
  (after-init . ready-player-mode))


;; Interaction with mpd
;; Don't work
(use-package mpc
  :disabled
  :general
  (k8x1d/leader-key
   "om"  '(mpc :which-key "Music player")
   )
  :bind
  ("C-c o m" . mpc)
  ;; :custom
  ;; (mpc-mpd-music-directory "~/Music")
  ;; (mpc-host "localhost:6600")
  ;; (mpc-host "127.0.0.1:6600")
  :config
  (setq mpc-mpd-music-directory "~/Music")
  (setq mpc-host "127.0.0.1:6600")
  ;; (setq mpc-host "127.0.0.1")
  )


;; Mpd interaction
(use-package mpdel
  :disabled
  :general
  (k8x1d/leader-key
    "M" '(:keymap mpdel-core-map
		  :which-key "Music")
   )
  :hook
  (after-init . mpdel-mode)
  )


(provide 'multimedia-module)
;;; multimedia-module.el ends here
