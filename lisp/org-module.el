;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; Inspiration
;; - https://sophiebos.io/posts/prettifying-emacs-org-mode/


;; Org look (general)
(use-package org
  :config
  (setq org-ellipsis " [...]") ;; change folding symbol
  (setq org-pretty-entities t) ;; pretiffy entities

  (setq org-adapt-indentation nil) ;; Indentation
  ;; Startup default
  (setq org-startup-folded 'fold)
  (setq
   ;; Edit settings
   org-auto-align-tags nil
   org-tags-column 0
   org-catch-invisible-edits 'show-and-error
   org-special-ctrl-a/e t
   org-insert-heading-respect-content t

   ;; Org styling, hide markup etc.
   org-hide-emphasis-markers t
   org-agenda-tags-column 0)


  ;; Styling
  (setq org-pretty-entities-include-sub-superscripts nil)

  ;; Src block
  (setq org-src-fontify-natively t
	org-src-tab-acts-natively t
	org-edit-src-content-indentation 0)
  (setq org-src-window-setup #'current-window)
  )

;; Adjust org look
(use-package org
  :hook
  (org-mode . visual-wrap-prefix-mode)
  ;; (org-mode . word-wrap-whitespace-mode)
  (org-mode . visual-line-mode)
  (org-mode . org-indent-mode)
  (org-mode . variable-pitch-mode)
  :config
  ;; Fontification
  (setq org-fontify-quote-and-verse-blocks nil
	org-fontify-done-headline t
	org-fontify-todo-headline nil
	org-fontify-emphasized-text t
	org-fontify-whole-heading-line nil
	org-fontify-whole-block-delimiter-line t)
  (setq org-adapt-indentation nil) ;; Indentation
  ;; (setq org-hide-leading-stars t) ;; hide bullet redundant bullets
  (setq org-hide-leading-stars nil) ;; hide bullet redundant bullets
  :custom-face
  ;; Force fixed pitch
  (org-block ((nil (:inherit 'fixed-pitch :height 0.85))))
  (org-checkbox ((nil (:inherit fixed-pitch))))
  (org-code ((nil (:inherit (shadow fixed-pitch) :height 0.85))))
  (org-indent ((nil (:inherit (org-hide fixed-pitch) :height 0.85))))
  (org-meta-line ((nil (:inherit (font-lock-comment-face fixed-pitch)))))
  (org-special-keyword ((nil (:inherit (font-lock-comment-face fixed-pitch)))))
  (org-table ((nil (:inherit 'fixed-pitch))))
  (org-verbatim ((nil (:inherit (shadow fixed-pitch) :height 0.85))))
  )


;; Modern org look
(use-package org-modern
  :disabled
  :hook
  (org-mode .  variable-pitch-mode)
  (org-mode .  org-modern-mode)
  (org-agenda-finalize . org-modern-agenda)
  :preface
  (defun k8x1d/set-org-colors ()
    (require 'org-modern)
    (require 'doom-themes)
    ;; TODOS faces
    (setq org-modern-todo-faces
	  `(
	    ("TODO" :background ,(doom-color 'red)
	     :foreground ,(doom-color 'bg)
	     :family "Iosevka Nerd Font"
	     :weight semibold)
	    ("NEXT" :background ,(doom-color 'green)
	     :foreground ,(doom-color 'bg)
	     :family "Iosevka Nerd Font"
	     :weight semibold)
	    ("WAIT" :background ,(doom-color 'yellow)
	     :foreground ,(doom-color 'bg)
	     :family "Iosevka Nerd Font"
	     :weight semibold)
	    ("DONE" :family "Iosevka Nerd Font"
	     :weight semibold) ;; uniformize fonts
	    ("CNCL"  :family "Iosevka Nerd Font"
	     :weight semibold) ;; uniformize fonts
	    ))
    (setq org-modern-priority-faces
	  `((?A :background ,(doom-color 'green)
		:foreground ,(doom-color 'bg)
		:family "Iosevka Nerd Font"
		:weight semibold)
	    (?B :background ,(doom-color 'orange)
		:foreground ,(doom-color 'bg)
		:family "Iosevka Nerd Font"
		:weight semibold)
	    (?C :background ,(doom-color 'red)
		:foreground ,(doom-color 'bg)
		:family "Iosevka Nerd Font"
		:weight semibold)))
    (setq org-modern-symbol
	  `((?X :background "darkgreen"
		:foreground "white"
		:family "Iosevka Nerd Font")
	    (?- :background "darkorange"
		:foreground "white"
		:family "Iosevka Nerd Font")
	    (?\s :background "darkred"
		 :foreground "white"
		 :family "Iosevka Nerd Font")))
    )
  :config
  ;; Bullets configuration 
  (setq org-modern-star 'fold)
  (setq org-modern-fold-stars '(("🞉" . "●")))
  ;; Checkbox
  (setq org-modern-checkbox '((?X . "󰄲 ")
			      (?- . "󱋬 ")
			      (?\s . "󰄮 ")))
  ;; List
  (setq org-modern-list '((43 . "◦")
			  ;; (45 . "-")
			  (45 . "‣")
			  ;; (45 . "►")
			  ;; (45 . "▶")
			  (42 . "•")))
  ;; Tables
  (setq org-modern-table nil)
  :custom-face
   (org-table ((nil (:inherit 'fixed-pitch :weight regular))))
   (org-table ((nil (:inherit 'fixed-pitch :weight regular))))
   (org-block ((nil (:inherit 'fixed-pitch :weight regular))))
   (org-modern-symbol ((nil (:inherit 'fixed-pitch :weight regular)))) ;; something probleme with unicode with arimo font
  )


;; Latex configuration
(use-package org
  :config
  (plist-put org-format-latex-options :scale 1.5)
  (setq org-latex-to-mathml-convert-command
	"latexmlmath \"%i\" --presentationmathml=%o")
  ;; (setq org-preview-latex-process-alist
  )


;; Manage markup appearing (custom)
(use-package org
  :disabled
  :preface
  (defun k8x1d/org-show-hide-markup ()
    (interactive)
    (setq-default org-startup-folded 'showeverything)
    (if org-hide-emphasis-markers
	(setq-default org-hide-emphasis-markers nil)
      (setq-default org-hide-emphasis-markers t))
    (org-mode-restart)
    )
  )

;; Manage markup appearing
(use-package org-appear
  ;; :hook
  ;; (org-mode . org-appear-mode)
  :config
  (setq org-hide-emphasis-markers t) ;; Must be activated for org-appear to work
  (setq org-appear-autoemphasis t) ;; Show bold, italics, verbatim, etc.
  (setq	org-appear-autolinks t) ;; Show links
  (setq	org-appear-autosubmarkers t) ;; Show sub- and superscripts
  )

;; Manage latex appearing
;; Interpret some box as latex when it shouldn't
(use-package org-fragtog
  :disabled
  :hook
  (org-mode-hook . org-fragtog-mode)
  )

;; GTD
(use-package org
  :general
  (k8x1d/leader-key
   "oa"  '(org-agenda :which-key "Agenda")
   "X"  '(org-capture :which-key "Capture")
   )
  ;; :commands (org-agenda org-capture)
  :bind
  ("C-c X" . org-capture)
  (:map k8x1d/leader-map
	("X" ("Capture" . org-capture))
	("oa" ("Agenda" . org-agenda))
	)
  :config
  ;; Capture
  (setq org-directory k8x1d/org-directory)
  (setq org-refile-targets
	'((nil :maxlevel . 3)
	  (org-agenda-files :maxlevel . 3)))
  (setq org-refile-use-outline-path t
	org-outline-path-complete-in-steps nil)
  (setq org-refile-use-cache t)
  (setq org-default-notes-file (concat k8x1d/notes-directory "/notes_frame13.org"))
  (setq org-capture-templates
	'(("t" "Todo" entry (file (lambda () (concat k8x1d/inbox-directory "/inbox-frame13.org")))
	   "* TODO %?\n")
	  ;; To write:
	  ;; see https://orgmode.org/manual/Capture-templates.html
	  ;; ("p" "Project" entry (file+function "~/org/projects.org" k8x1d/get-project-name)
	  ;;  "*  %?\n  %i\n")
	  ;; Calendars
	  ("c" "Calendars")
	  ("cr" "Recherches" entry (file (lambda () (concat k8x1d/calendars-directory "/recherche.org"))) "* %?\n%^T")
	  ("cs" "Social" entry (file (lambda () (concat k8x1d/calendars-directory "/social.org"))) "* %?\n%^T")
	  ("ci" "Implications" entry (file (lambda () (concat k8x1d/calendars-directory "/implications.org"))) "* %?\n%^T")
	  ("cf" "Formations" entry (file (lambda () (concat k8x1d/calendars-directory "/formations.org"))) "* %?\n%^T")
	  ("cE" "Exercices" entry (file (lambda () (concat k8x1d/calendars-directory "/exercices.org"))) "* %?\n%^T")
	  ("ce" "Emplois" entry (file (lambda () (concat k8x1d/calendars-directory "/emplois.org"))) "* %?\n%^T")
	  ("cd" "Développement" entry (file (lambda () (concat k8x1d/calendars-directory "/developpements.org"))) "* %?\n%^T")
	  ;; Notes
	  ("n" "Notes" entry (file (lambda () (concat k8x1d/notes-directory "/notes_frame13.org"))) "* %U %?\n")
	  ))

  ;; Don't break window layout when suggestion todo state option
  (setq org-use-fast-todo-selection 'expert)

  (setq org-enforce-todo-dependencies t)
  (setq org-enforce-todo-checkbox-dependencies t)

  (setq org-todo-keywords
	'((sequence "TODO(t)" "NEXT(n)" "WAIT(w)" "|" "DONE(d)" "CNCL(c)")))
  )

;; Agenda configuration
(use-package org-agenda
  :bind
  ("C-c o a" . org-agenda)
  :config
  (setq org-agenda-files k8x1d/org-agenda-files)

  ;; Don't show tasks if already done
  ;; (setq org-agenda)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-skip-timestamp-if-done t)
  (setq org-agenda-skip-function-global '(org-agenda-skip-entry-if 'todo 'done)) ;; really ensure that done are not shown, see https://stackoverflow.com/questions/8281604/remove-done-tasks-from-agenda-view

  ;; Habit configuration
  (setq org-habit-preceding-days 7)
  (setq org-habit-following-days 3)
  (setq org-habit-graph-column 17)

  ;; Reduce redundancy
  (setq org-agenda-skip-deadline-prewarning-if-scheduled t)
  (setq org-agenda-skip-scheduled-delay-if-deadline t)
  (setq org-agenda-skip-scheduled-if-deadline-is-shown t)
  (setq org-agenda-skip-scheduled-repeats-after-deadline t)
  (setq org-agenda-skip-timestamp-if-deadline-is-shown t)
  (setq org-agenda-show-future-repeats t)
  (setq org-deadline-warning-days 0) ;; don't show upcoming deadline in today agenda

  ;; Initial agenda
  (setq org-agenda-span 'week) ;; span
  (setq org-agenda-inhibit-startup t)
  (setq org-agenda-window-setup 'current-window) ;; take the whole buffer
  (setq org-agenda-start-on-weekday 1) ;; start agenda on monday (ISO 8601, see https://en.wikipedia.org/wiki/ISO_8601)

  (setq org-agenda-prefix-format '(
				   (agenda . " %i %-15c%-18t [󰔟%-4s |󰿖 %-3e] ")
				   (todo . " %i %-12:c")
				   (tags . " %i %-12:c")
				   (search . " %i %-12:c")
				   ))
  (setq org-agenda-todo-keyword-format "%-4s ")
  (setq org-agenda-log-mode-items '(closed))
  (setq org-agenda-sorting-strategy '((agenda time-up priority-down category-up tag-up habit-down )))
  )

;; Allow drag-and-drop behavior to org
(use-package org-download
  :init
  (setq org-download-image-dir "~/org/screenshots")
  (setq org-download-method 'directory)
  )

;; Timer configuration
(use-package org
  :preface
  (defun k8x1d/timer-set-timer ()
    (interactive)
    (async-shell-command (format "sleep %s; notify-send \"Timer\" \"done\"" (read-string "Enter time:")))
    )
   :config
   (setq org-timer-display nil)
  ;; (add-to-list 'global-mode-string 'org-timer-mode-line-string)
  )

;; UTF Bullets
(use-package org-superstar
  :hook
  (org-mode . org-superstar-mode)
  :config
  ;; see https://symbl.cc/en/search/?q=bullet
  (setq org-superstar-headline-bullets-list '(9673 9678 9675))
  ;; (setq org-superstar-headline-bullets-list '(9312 9313 9314 9315 9316 9317 9318 9319 9320))
  (setq org-superstar-item-bullet-alist '((42 . 8226) (43 . 10148) (45 . 8259)))
  (setq org-superstar-special-todo-items nil)
  :custom-face
  (org-superstar-header-bullet ((t (:inherit 'fixed-pitch))))
  (org-superstar-item ((t (:inherit 'fixed-pitch))))
  )

;; Automatic table of content
(use-package toc-org
  :hook
  (org-mode . toc-org-mode))

;; Show inline images as sliced image (enhance scrolling experience)
;; Stopped working... Repair...
(use-package org-sliced-images
  :disabled
  :hook
  (org-mode . org-sliced-images-mode)
  )

;; Org clock
;; FIXME: cause emacs to freeze
(use-package org
  :config
  (setq org-show-notification-timeout 10)
  (setq org-clock-sound k8x1d/org-clock-sound)
  )


;; Corrector
(use-package lsp-bridge
  :if (string= k8x1d/lsp-provider "lsp-bridge")
  :ensure nil
  :load-path "packages/lsp-bridge"
  :hook
  (org-mode . lsp-bridge-mode)
  :config
  (add-to-list 'lsp-bridge-single-lang-server-mode-list
	       '((org-mode) . "ltex-ls"))
  )

(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :init
  (setq eglot-connect-timeout 180)
  :config
  ;; set lsp program
  (add-to-list 'eglot-server-programs
	       '(org-mode .
			  ("/home/k8x1d/.config/emacs/.cache/eglot/ltex-ls/ltex-ls-15.2.0/bin/ltex-ls")
			  ))
  )


(provide 'org-module)
;;; org-module.el ends here
