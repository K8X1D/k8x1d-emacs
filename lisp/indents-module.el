;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; Automatic reindent
(use-package electric
  :hook
  (after-init . electric-indent-mode)
  )


(provide 'indents-module)
;;; indents-module.el ends here
