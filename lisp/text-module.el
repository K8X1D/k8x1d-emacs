;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Visualize whitespace
;; Noisy when on, 
;; (use-package whitespace
;;   :hook
;;   (text-mode . whitespace-mode)
;;   )

;; Delete selected region when editing over it
(use-package  delsel
  :hook
  (after-init . delete-selection-mode)
  )

;; Wrap text
(use-package text-mode
  :hook
  ;; ‘word-wrap’ is turned on in this buffer, and simple editing commands are redefined to act on visual lines, not logical lines.
  ;; Wrap line
  (text-mode . visual-line-mode)
  ;; Better wrapping,  The characters to break on are defined by ‘word-wrap-whitespace-characters’.
  ;; (text-mode . word-wrap-whitespace-mode)
  ;; Display continuation lines with prefixes from surrounding context.
  ;; (text-mode . visual-wrap-prefix-mode)
  )



(provide 'text-module)
;;; text-module.el ends here
