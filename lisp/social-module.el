;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Mastodon interaction
(use-package mastodon
  :general
  (k8x1d/leader-key
   "o"  '(:ignore t :which-key "Open")
   "oS"  '(mastodon :which-key "Social")
    )
  ;; :bind ("C-c o M"  . mastodon)
  :config
  (setq mastodon-instance-url "https://mastodon.online"
	mastodon-active-user "k8x1d")
  ;; (mastodon-discover)
  )

(provide 'social-module)
;;; social-module.el ends here
