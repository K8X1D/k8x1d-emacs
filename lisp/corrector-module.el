;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Via LTeX (Manual)
(use-package eglot
  :if (string= k8x1d/lsp-provider "eglot")
  :init
  (setq eglot-connect-timeout 180)
  :hook
  (message-mode . eglot-ensure) ;; lsp
  :config
  (add-to-list 'eglot-server-programs
	       '(message-mode .
			     ("/home/k8x1d/.config/emacs/.cache/eglot/ltex-ls/ltex-ls-15.2.0/bin/ltex-ls")
			     ))
  )

;; Via LTeX (auto)
(use-package eglot-ltex
  :if (string= k8x1d/lsp-provider "eglot")
  :disabled
  ;; :hook
  ;; ((LaTeX-mode message-mode org-src-mode org-mode markdown-mode forge-post-mode) . (lambda ()
									    ;; (require 'eglot-ltex)
									    ;; (eglot-ensure)))
  :init
  (setq eglot-ltex-server-path "/home/k8x1d/.config/emacs/.cache/eglot/ltex-ls/ltex-ls-15.2.0")
  (setq eglot-connect-timeout 180)
  (setq eglot-ltex-communication-channel 'stdio)
  (defun k8x1d/launch-corrector ()
    (interactive)
    (require 'eglot-ltex)
    (eglot-ensure))

  ;; Set general config for ltex
  (setq-default eglot-workspace-configuration
		;; '(:ltex (:language "auto")) ;; don't work
		'(:ltex (:language "fr"))
		)
  )

;; Via Languagetool (auto)
(use-package flymake-languagetool
  :disabled
  :hook ((text-mode       . flymake-languagetool-load)
         (latex-mode      . flymake-languagetool-load)
	 (org-mode        . flymake-languagetool-load)
	 (markdown-mode   . flymake-languagetool-load))
  :init
  ;; LanguageTools API Remote Server Configuration
  (setq flymake-languagetool-server-jar nil)
  (setq flymake-languagetool-language "fr")
  ;; (setq flymake-languagetool-url "http://localhost:8081")
  :config
  (setq flymake-languagetool-check-spelling t)
  (setq flymake-languagetool-ignore-faces-alist
	'((org-mode . (org-code
		       org-date
		       org-tag
		       org-block
		       org-block-begin-line
		       org-block-end-line
		       org-todo
		       org-table
		       org-property-value
		       org-special-keyword
		       org-level-1
		       org-level-2
		       org-level-3
		       org-level-4
		       org-level-5
		       org-level-6
		       org-level-7
		       org-level-8
		       org-drawer
		       org-superstar-leading
		       org-superstar-item
		       org-superstar-header-bullet
		       ))
	  (markdown-mode . (markdown-code-face
			    markdown-inline-code-face markdown-pre-face
			    markdown-url-face markdown-plain-url-face
			    markdown-math-face markdown-html-tag-name-face
			    markdown-html-tag-delimiter-face
			    markdown-html-attr-name-face
			    markdown-html-attr-value-face
			    markdown-html-entity-face))))
  )

;; Via Languagetool (no dependence)
;; Clumsy...
(use-package langtool
  :disabled
  :config
  (setq langtool-http-server-host "localhost"
	langtool-http-server-port 8081)
  )

(provide 'corrector-module)
;;; corrector-module.el ends here
