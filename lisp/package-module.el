;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Package configuration
(use-package use-package-core
  :config
  (setq use-package-enable-imenu-support t)
  (setq use-package-compute-statistics t)
  ;; Defer if no daemon, demand if daemon
  (if (daemonp)
      (progn
	(message "Loading in the daemon!")
	(setq use-package-always-demand t))
	;;(setq use-package-always-defer 3))
    (progn
      (message "Loading in regular Emacs!")
      (setq use-package-always-defer t)))
  )

(provide 'package-module)
;;; package-module.el ends here
