;;; .* --- .*"  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;; Other
(defvar k8x1d/timed-startup nil)

;; Org
(defvar k8x1d/org-directory "~/org")
(defvar k8x1d/calendars-directory (concat k8x1d/org-directory "/Calendars"))
(defvar k8x1d/inbox-directory (concat k8x1d/org-directory "/Inbox"))
(defvar k8x1d/journal-directory (concat k8x1d/org-directory "/Journals"))
(defvar k8x1d/org-agenda-files (list (concat k8x1d/org-directory "/Projects")
				     (concat k8x1d/org-directory "/Projets_rewrite")
				     k8x1d/calendars-directory
				     k8x1d/inbox-directory))
(defvar k8x1d/org-clock-sound "/home/k8x1d/Music/library/Cuts/hourly-dong-looped.wav")

;; Citar
(defvar k8x1d/bibliography '("~/Zotero/k8x1d.bib" "/extension/Data/Cloud/facil/Bibliography/bibliography.bib")) ;; Defaults bibliographies
(defvar k8x1d/bibliography-notes '("~/Zotero/notes")) ;; Defaults notes for bibliographies

;; Denote
(defvar k8x1d/notes-directory (concat k8x1d/org-directory "/Notes"))
(defvar k8x1d/journal-directory (concat k8x1d/org-directory "/Journals"))
(defvar k8x1d/denote-exploration-directory (concat k8x1d/notes-directory "/explore"))

;; Project
;; (add-to-list 'k8x1d/org-agenda-files '"/home/k8x1d/Documents/Recherche/Doctorat/These/these/todos.org")
;; (add-to-list 'k8x1d/org-agenda-files '"/home/k8x1d/Documents/Recherche/Presentations/2025/CIRST/todos.org")
 

;; LSP
;; (defvar k8x1d/lsp-provider "lsp-bridge")
(defvar k8x1d/lsp-provider "eglot")


(provide 'variables-module)
;;; variables-module.el ends here
